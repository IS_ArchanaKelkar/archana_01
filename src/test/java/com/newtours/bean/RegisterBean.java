package com.newtours.bean;

import com.qmetry.qaf.automation.data.BaseDataBean;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class RegisterBean extends BaseDataBean {
	
	@Randomizer(length=15,type=RandomizerTypes.LETTERS_ONLY)
	private String firstname,lastname,city,state,country;
		
	@Randomizer(length=10,type=RandomizerTypes.DIGITS_ONLY)
	private String phoneno,postal;
	
	@Randomizer(length=15,type=RandomizerTypes.MIXED, suffix="@gmail.com")
	private String emailid;
	
	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getCountry() {
		return country;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public String getPostal() {
		return postal;
	}

	public String getEmailid() {
		return emailid;
	}

	public String getAddress1() {
		return address1;
	}

	public String getAddress2() {
		return address2;
	}
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	@Randomizer(length=50,type=RandomizerTypes.MIXED)
	private String address1,address2;
	
	@Randomizer(length=8,type=RandomizerTypes.MIXED)
	private String username;
	
	@Randomizer(length=15,type=RandomizerTypes.MIXED)
	private String password;
	
	

}
