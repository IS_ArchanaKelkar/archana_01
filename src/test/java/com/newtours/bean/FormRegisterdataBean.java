package com.newtours.bean;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.ui.annotations.UiElement.Type;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Randomizer;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;

public class FormRegisterdataBean extends BaseFormDataBean {
	
	@Randomizer(length=15,type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="register.firstname.text")
	private String firstname;
	
	
	@Randomizer(length=15,type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="register.lastname.text")
	private String lastname;
	

	@Randomizer(length=10,type=RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="register.phone.text")
	private String phone;
	
	@Randomizer(length=10,type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="register.city.text")
	private String city;
	
	@Randomizer(length=10,type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="register.state.text")
	private String state;
	
	@Randomizer(length=10,type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="register.country.text", fieldType=Type.selectbox)
	private String country;
	
	@Randomizer(length=10,type=RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="register.postalcode.text", fieldType=Type.textbox)
	private String postalcode;
		
	@Randomizer(length=15,type=RandomizerTypes.MIXED, suffix="@gmail.com")
	@UiElement(fieldLoc="register.email.text", fieldType=Type.textbox)
	private String email;
	
	@Randomizer(length=50,type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc="register.address1.text" )
	private String address1;
	
	@Randomizer(length=50,type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc="register.address2.text")
	private String address2;
	
	@Randomizer(length=8,type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc="register.username.text")
	private String username;
	
	@Randomizer(length=15,type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc="register.password.text")
	private String password;
	
	@Randomizer(length=15,type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc="register.confpassword.text")
	private String confpassword;

}
