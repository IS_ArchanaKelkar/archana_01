package com.newtours.bean;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.ui.annotations.UiElement.Type;
import com.qmetry.qaf.automation.util.Randomizer;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;

public class FlightFinderBean extends BaseFormDataBean {
	
	@Randomizer(length=15,type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="flightfinder.triptype.radio",fieldType=Type.optionbox)
	private String triptype;
	
	@Randomizer(length=1,type=RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="flightfinder.nopassenger.dropdown",fieldType=Type.selectbox)
	private int nopassenger;
	
	@Randomizer(length=10,type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="flightfinder.depfrom.dropdown",fieldType=Type.selectbox)
	private String depfrom;
	
	@Randomizer(length=10,type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="flightfinder.depmonth.dropdown",fieldType=Type.selectbox)
	private String depmonth;
	
	@Randomizer(length=10,type=RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="flightfinder.depday.dropdown",fieldType=Type.selectbox)
	private String depday;
	
	@Randomizer(length=10,type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="flightfinder.tomonth.dropdown",fieldType=Type.selectbox)
	private String tomonth;
	
	@Randomizer(length=10,type=RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc="flightfinder.today.dropdown",fieldType=Type.selectbox)
	private String today;
	
	@Randomizer(length=10,type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="flightfinder.depto.dropdown",fieldType=Type.selectbox)
	private String depto;
	
	@Randomizer(length=10,type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="flightfinder.serviceclass.radio", fieldType=Type.optionbox)
	private String servclass;
	
	@Randomizer(length=10,type=RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc="flightfinder.airline.dropdown", fieldType=Type.selectbox)
	private String airline;
		
	@Randomizer(length=15,type=RandomizerTypes.MIXED)
	@UiElement(fieldLoc="flightfinder.continue.button")
	private String flightcontinue;

}
