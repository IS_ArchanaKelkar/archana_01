package com.newtours.components;

import java.util.List;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FlightComponent extends QAFWebComponent {

	public FlightComponent(String locator) {
		super(locator);
	}
	
	@FindBy(locator="selectflight.flightname.label")
	private List<QAFWebElement> flightname;
		
	@FindBy(locator="selectflight.flightprice.label")
	private List<QAFWebElement> flightprice;
		
	@FindBy(locator="selectflight.flight.radio")
    private List<QAFWebElement> flight;
	
	
	public List<QAFWebElement> getFlightname() {
		return flightname;
	}

	public List<QAFWebElement> getFlightprice() {
		return flightprice;
	}

	public List<QAFWebElement> getFlight() {
		return flight;
	}
}
