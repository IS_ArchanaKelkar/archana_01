package com.newtours.test;

import java.util.Map;

import org.testng.annotations.Test;

import com.newtours.pages.FieldValidationBookFlightPage;
import com.newtours.pages.FlightConfirmation;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Validator;


public class FieldValidationBookFlightTest extends WebDriverTestCase {
	
	@QAFDataProvider(dataFile="resources/testdata/bookflightdetail.xls",key="key1")
	@Test(description="xls user login")
	public void QAFLogin(Map<String,String>data) {
		FlightConfirmation fr=new FlightConfirmation();
		fr.launchPage(null);
		fr.doLogin(data.get("username"),data.get("password"));
		fr.validateinputfields(data);
		String Tax=fr.getTaxtoNext()+ " USD";
		String Totalprice=fr.getprice()+ " USD";;
		fr.getBuyFlights().click();
		String title=getDriver().getTitle();
        Validator.verifyTrue(title.equals("Flight Confirmation: Mercury Tours"), "failure", "success");
        String deptingplace=data.get("depfrom")+" to " +data.get("depto");
        String returningplace=data.get("depto")+" to " +data.get("depfrom");
        String nop=data.get("nopassenger")+ " passenger";
        Validator.verifyTrue(fr.getDeparting().getText().equals(deptingplace),"failure", "success");
        System.out.println(fr.getNoofpassengers().getText());
        Validator.verifyTrue(fr.getReturning().getText().equals(returningplace),"failure", "success");
        Validator.verifyTrue(fr.getNoofpassengers().getText().equals(nop),"failure", "success");
        
        String billingaddress=data.get("Address")+"\n\n"+data.get("City")+", "+data.get("State")+", "+data.get("Zip")+"\n"+"AX 0";
        String deliveryaddress=data.get("Address")+"\n\n"+data.get("City")+", "+data.get("State")+", "+data.get("Zip");
       
        Validator.verifyTrue(fr.getBillingAddress().getText().equals(billingaddress),"failure", "success");
        Validator.verifyTrue(fr.getDeliaddress().getText().equals(deliveryaddress),"failure", "success");
        Validator.verifyTrue(fr.getNoofpassengers().getText().equals(nop),"failure", "success");
        
        Validator.verifyTrue(fr.getTaxinfo().getText().equals(Tax), "failure","Success");
        Validator.verifyTrue(fr.getTotalprice().getText().equals(Totalprice), "failure","Success");
        
        fr.getLogout().click();
        String lasttitle=getDriver().getTitle();
		Validator.verifyTrue(lasttitle.equals("Sign-on: Mercury Tours"), "Failed login","success");

	}
}
