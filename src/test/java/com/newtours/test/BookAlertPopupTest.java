package com.newtours.test;

import java.util.Map;

import org.testng.annotations.Test;

import com.newtours.pages.FieldValidationBookFlightPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class BookAlertPopupTest extends WebDriverTestCase {
	
	@QAFDataProvider(key="bookflight.credpopup")
	@Test(description="xml user login")
	public void QAFLogin(Map<String,String>data) {
		FieldValidationBookFlightPage fr=new FieldValidationBookFlightPage();
		fr.launchPage(null);
		fr.doLogin(data.get("username"),data.get("password"));
		fr.alertdataverification(data);
     	Validator.verifyTrue(fr.getAlert(), "fail", "Pass");
		Reporter.logWithScreenShot("Successful");
		
	}

}
