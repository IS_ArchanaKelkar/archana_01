package com.newtours.test;

import java.util.Map;

import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.newtours.pages.FlightFinder;
import com.newtours.pages.SelectFlightPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class SameFromAndToLocation extends WebDriverTestCase{

		@QAFDataProvider(dataFile="resources/testdata/credentials.xls",sheetName="credentials",key="Key1")
		@Test(description="xls user login")
		public void QAFLogin(Map<String,String>data1) {
			FlightFinder fr=new FlightFinder();
			fr.launchPage(null);
			getDriver().manage().window().maximize();
			fr.doLogin(data1.get("username"),data1.get("password"));
			fr.findFlights();
	   		String title=getDriver().getTitle();
			Validator.verifyTrue(title.equals("Select a Flight: Mercury Tours"), "Failed login","success");
			Reporter.logWithScreenShot("Success");
		}
	}
