package com.newtours.test;

import java.util.Map;

import org.testng.annotations.Test;

import com.newtours.pages.RegisterPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class RegisterTest extends WebDriverTestCase{
	
	@QAFDataProvider(dataFile="resources/testdata/invalidtestdata.xls",sheetName="Sheet1",key="Key1")
	@Test(description="xls file input")
	public void register(Map<String,String>data)
	{
	RegisterPage rp=new RegisterPage();
	rp.launchPage(null);
	rp.registerUser();
	rp.registerInvalidUser(data);
	String url=getDriver().getTitle();
	Validator.verifyTrue(url.equals("Register: Mercury Tours"), "invalid login", "Successful Login");
	rp.getMessage1().assertPresent("Dear " +rp.getFirstname()+ " "+ rp.getLastname()+ ",");
	rp.getMessage2().assertPresent("Thank you for registering. You may now sign-in using the user name and password you've just entered.");
	System.out.println("Success");
	}
	
}
