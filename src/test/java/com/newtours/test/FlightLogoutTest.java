package com.newtours.test;

import java.util.Map;

import org.testng.annotations.Test;

import com.newtours.pages.FlightLogout;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.PropertyUtil;
import com.qmetry.qaf.automation.util.Validator;

public class FlightLogoutTest extends WebDriverTestCase {
	@QAFDataProvider(dataFile="resources/testdata/credentials.xls",sheetName="credentials",key="Key1")
	@Test(description="xls user login")
	public void QAFLogin(Map<String,String>data1) {
		FlightLogout fr=new FlightLogout();
		fr.launchPage(null);
		fr.doLogin(data1.get("username"),data1.get("password"));
		fr.multipleclicks();
		String title=getDriver().getTitle();
		Validator.verifyTrue(title.equals("Sign-on: Mercury Tours"), "Failed login","success");

	}
}
