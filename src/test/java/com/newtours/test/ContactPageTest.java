package com.newtours.test;

import java.util.Map;

import org.testng.annotations.Test;

import com.newtours.pages.ContactPage;
import com.newtours.pages.HomePage;
import com.newtours.pages.SignOnPage;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Validator;

public class ContactPageTest extends WebDriverTestCase{
	
	@Test(description="Contact Page Verification")
	public void verify() {
		ContactPage cp=new ContactPage();
		cp.launchPage(null);
		cp.doClick();
		String title=getDriver().getTitle();
		Validator.verifyTrue(title.equals("Welcome: Mercury Tours"), "Failed login","success");
	}

}
