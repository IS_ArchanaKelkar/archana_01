package com.newtours.test;


import java.util.Map;

import org.testng.annotations.Test;

import com.newtours.pages.*;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Validator;

public class SignOnTest extends WebDriverTestCase {

	@QAFDataProvider(key="login.data")
	@Test(description="xml user login")
	public void QAFLogin(Map<String,String>data1) {
		SignOnPage sop=new SignOnPage();
		sop.launchPage(null);
		sop.doLogin(data1.get("username"),data1.get("password"));
		String title=getDriver().getTitle();
		Validator.verifyTrue(title.equals("Sign-on: Mercury Tours"), "Failed login","success");

	}
}
