package com.newtours.test;

import org.testng.annotations.Test;

import com.newtours.pages.HomePage;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.PropertyUtil;

public class LoginPropertyFileTest extends WebDriverTestCase{
	
	@Test
	public void verifyhomepage()
	{
		HomePage hpl=new HomePage();
		hpl.launchPage(null);
		/*PropertyUtil prop=new PropertyUtil("resources\\properties\\login.properties");
		hpl.doLogin(prop.getPropertyValue("username"),prop.getPropertyValue("password"));*/
		hpl.doLogin(ConfigurationManager.getBundle().getString("username"),ConfigurationManager.getBundle().getString("password"));
		hpl.verifyContents();
	}

}
