package com.newtours.test;

import java.util.Map;

import org.jboss.netty.util.internal.SystemPropertyUtil;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.newtours.pages.FieldValidationBookFlightPage;
import com.newtours.pages.FlightConfirmation;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Validator;

public class FindFlightsTest extends WebDriverTestCase{

	@QAFDataProvider(key="data.flight.flightdetails")
	@Test(description="xml user login")
	public void QAFLogin(Map<String,String>data1) {
		FlightConfirmation fr=new FlightConfirmation();
		fr.launchPage(null);
		getDriver().manage().window().maximize();
		fr.doLogin(data1.get("username"),data1.get("password"));
		fr.findFlight();
		String nop=new Select(fr.getNopassenger()).getFirstSelectedOption().getText().trim();
	    fr.getFlightcontinue().click();
		
		fr.selectFlight(data1);
		String deprate=fr.getRate(fr.selectRate(data1.get("departure")).getText());
		String returnrate=fr.getRate(fr.selectRate(data1.get("return")).getText());
		fr.getSelectflights().click();
		String s=getDriver().getTitle();
		String departure=data1.get("depfrom")+ " to "+ data1.get("depto");
		String ret=data1.get("depto")+ " to "+ data1.get("depfrom");
		
		Validator.verifyTrue(s.equals("Book a Flight: Mercury Tours"),"Failed to Select flight", "Successful Selection");
	
		Reporter.log(departure+ " "+fr.getDeparturedate().getText());
		Reporter.log("Flight -"+ data1.get("departure"));
		Reporter.log("Class -"+data1.get("Class"));
		Reporter.log("Price -"+ deprate);
		
		Reporter.log(ret+ " "+fr.getRetrundate().getText());
		Reporter.log("Flight -"+ data1.get("return"));
		Reporter.log("Class -"+data1.get("Class"));
		Reporter.log("Price -"+ returnrate);
		
		Reporter.log("Passengers :"+nop);
		Reporter.log("Taxes :"+ fr.getTaxtoNext());
		Reporter.log("Total Price (including taxes) :" +fr.getprice());
		
		Validator.verifyTrue(fr.getDeparture().getText().equals(departure), "Failed", "Success");
		Validator.verifyTrue(fr.getReturns().getText().equals(ret), "Failed", "Success");
	
		Validator.verifyTrue(fr.getDeparturedate().getText().equals(fr.getDepartureDate()), "Failed", "Success");
		Validator.verifyTrue(fr.getRetrundate().getText().equals(fr.getReturnDate()), "Failed", "Success");
		
		Validator.verifyTrue(fr.getDepartureclass().getText().equals(data1.get("Class")), "Failed", "Success");
		Validator.verifyTrue(fr.getReturnclass().getText().equals(data1.get("Class")), "Failed", "Success");
  
		Validator.verifyTrue(fr.getDeparturerate().getText().equals(deprate), "fail", "success");
		Validator.verifyTrue(fr.getReturnrate().getText().equals(returnrate), "fail", "success");
		
		Validator.verifyTrue(fr.getPasscount().getText().equals(nop), "fail", "success");

		fr.bookFlight(data1);
		String Tax=fr.getTaxtoNext()+ " USD";
		String Totalprice=fr.getprice()+ " USD";;

		fr.getBuyFlights().click();
		String title=getDriver().getTitle();
		
        Validator.verifyTrue(title.equals("Flight Confirmation: Mercury Tours"), "failure", "success");
        Validator.verifyTrue(fr.getDeparting().getText().equals(departure),"failure", "success");
        Validator.verifyTrue(fr.getReturning().getText().equals(ret),"failure", "success");
        Validator.verifyTrue(fr.getNoofpassengers().getText().equals(nop+" passenger"),"failure", "success");
       
        String billingaddress=data1.get("credfirstname")+" "+data1.get("credmidname")+" "+data1.get("credlastname")+
        		"\n"+data1.get("bAddress")+"\n\n"+data1.get("bcity")+", "+data1.get("bstate")+", "+data1.get("bzip")+"\n"+"AX 0";
     
        String deliveryaddress=data1.get("dAddress")+"\n\n"+data1.get("dCity")+", "+data1.get("dState")+", "+data1.get("dZip");
        
        Validator.verifyTrue(fr.getBillingAddress().getText().equals(billingaddress),"failure", "success");
        Validator.verifyTrue(fr.getDeliaddress().getText().equals(deliveryaddress),"failure", "success");
          
        Validator.verifyTrue(fr.getTaxinfo().getText().equals(Tax), "failure","Success");
        Validator.verifyTrue(fr.getTotalprice().getText().equals(Totalprice), "failure","Success");

		
	}
}
