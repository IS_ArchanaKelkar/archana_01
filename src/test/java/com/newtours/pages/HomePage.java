package com.newtours.pages;

import java.util.List;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

import static com.qmetry.qaf.automation.step.CommonStep.*;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	@FindBy(locator="homepage.toplink.link")
	private List<QAFWebElement> toplinks;
	
	@FindBy(locator="homepage.leftlink.link")
	private List<QAFWebElement> leftlinks;
	
	@FindBy(locator="homepage.contentlink.link")
	private List<QAFWebElement> contlinks;
	
	@FindBy(locator="homepage.rightcontent.image")
	private List<QAFWebElement> rightcontent;
	
	@FindBy(locator="homepage.component.frame")
	private List<QAFWebElement> component;
	
	@FindBy(locator="homepage.label.image")
	private List<QAFWebElement> label;
	 
	

	public void verifyContents()
	{
		 for(QAFWebElement ele:toplinks) {
			 ele.verifyVisible();
		 }
		 for(QAFWebElement ele:leftlinks) {
			 ele.verifyVisible();
		 }
		 for(QAFWebElement ele:contlinks) {
			 ele.verifyVisible();
		 }
		 for(QAFWebElement ele:rightcontent) {
			 ele.verifyVisible();
		 }
		 for(QAFWebElement ele:component) {
			 ele.verifyVisible();
		 }
		 for(QAFWebElement ele:label) {
			 ele.verifyVisible();
		 }
			
	}
	public List<QAFWebElement> getToplinks() {
		return toplinks;
	}
	public List<QAFWebElement> getLeftlinks() {
		return leftlinks;
	}
	public List<QAFWebElement> getContlinks() {
		return contlinks;
	}
	public List<QAFWebElement> getRightcontent() {
		return rightcontent;
	}
	public List<QAFWebElement> getComponent() {
		return component;
	}
	public List<QAFWebElement> getLabel() {
		return label;
	}
	@FindBy(locator="login.username.text")
	private QAFWebElement username;
	
	@FindBy(locator="login.password.text")
	private QAFWebElement password;
	
	@FindBy(locator="login.login.btn")
	private QAFWebElement login;
	
	@FindBy(locator="homepage.signon.btn")
	private QAFWebElement signon;
	
	@FindBy(locator="homepage.contact.btn")
	private QAFWebElement contact;

	public QAFWebElement getContact() {
		return contact;
	}
	public QAFWebElement getUsername() {
		return username;
	}

	public QAFWebElement getPassword() {
		return password;
	}

	public QAFWebElement getLogin() {
		return login;
	}

	public QAFWebElement getSignon() {
		return signon;
	}
	@QAFTestStep(description="Login")
	public void doLogin(String uname,String pwd)
	{
		getUsername().sendKeys(uname);
		getPassword().sendKeys(pwd);
		getLogin().click();
	}
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");
	}

}
