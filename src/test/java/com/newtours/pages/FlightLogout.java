package com.newtours.pages;

import org.testng.annotations.Test;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FlightLogout extends HomePage {

	@FindBy(locator="flightfinder.continue.button")
	private QAFWebElement flightcontinue;
	
	@FindBy(locator="selectflight.continue.button")
	private QAFWebElement selectflights;
	
	@FindBy(locator="bookflight.securepurchase.button")
	private QAFWebElement buyflights;
	
	@FindBy(locator="flightconfirmation.logout.button")
	private QAFWebElement logout;
	
	@FindBy(locator="flightconfirmation.backto.button")
	private QAFWebElement backtobutton;
	

	public QAFWebElement getBacktobutton() {
		return backtobutton;
	}


	public QAFWebElement getFlightcontinue() {
		return flightcontinue;
	}


	public QAFWebElement getSelectflights() {
		return selectflights;
	}


	public QAFWebElement getBuyflights() {
		return buyflights;
	}


	public QAFWebElement getLogout() {
		return logout;
	}


	@QAFTestStep(description="steps to reach flightconfirmation page for logout")
	public void multipleclicks()
	{ 
		getFlightcontinue().click();
		getSelectflights().click();
		getBuyflights().click();
		getLogout().click();
	}
	@QAFTestStep(description="steps to reach flightconfirmation page for back to flights button")
    public void flightconfirmation()
    {
    	getFlightcontinue().click();
		getSelectflights().click();
		getBuyflights().click();	
		getBacktobutton().click();
    }
}
