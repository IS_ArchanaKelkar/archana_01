package com.newtours.pages;

import java.time.LocalDate;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.support.ui.Select;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class FieldValidationBookFlightPage extends SelectFlightPage{

	@FindBy(locator="selectflight.depart.radio")
	private QAFWebElement departbutton;
	
	@FindBy(locator="selectflight.return.radio")
	private QAFWebElement returnbutton;

	public QAFWebElement getDepartbutton() {
		return departbutton;
	}
	
	@FindBy(locator="bookflight.passcount.label")
	private QAFWebElement passcount;
	
	public QAFWebElement getPasscount() {
		return passcount;
	}
	
	@FindBy(locator="bookflight.departure.label")
	private QAFWebElement departure;
	
	@FindBy(locator="bookflight.departureflightname.label")
	private QAFWebElement departflightname;
	
	
	@FindBy(locator="bookflight.returnflight.label")
	private QAFWebElement returns;
	
	@FindBy(locator="bookflight.returnflightname.label")
	private QAFWebElement returnflightname;
	
	public QAFWebElement getDeparture() {
		return departure;
	}

	public QAFWebElement getDepartflightname() {
		return departflightname;
	}

	public QAFWebElement getReturns() {
		return returns;
	}

	public QAFWebElement getReturnflightname() {
		return returnflightname;
	}

	@FindBy(locator="bookflight.creditcard.dropdown")
	private QAFWebElement creditcard;
	
	@FindBy(locator="bookflight.billingaddress.text")
	private QAFWebElement billingaddress;
	
	
	@FindBy(locator="bookflight.billingcity.text")
	private QAFWebElement billingcity;
	
	@FindBy(locator="bookflight.billingstate.text")
	private QAFWebElement billingstate;
	
	@FindBy(locator="bookflight.billingzip.text")
	private QAFWebElement billingzip;
	
	@FindBy(locator="bookflight.billingccountry.dropdown")
	private QAFWebElement billingcountry;
	
	
	@FindBy(locator="bookflight.deladdress.text")
	private QAFWebElement deladdress;
	
	
	@FindBy(locator="bookflight.delcity.text")
	private QAFWebElement delcity;
	
	@FindBy(locator="bookflight.delstate.text")
	private QAFWebElement delstate;
	
	@FindBy(locator="bookflight.delzip.text")
	private QAFWebElement delzip;
	
	@FindBy(locator="bookflight.delccountry.dropdown")
	private QAFWebElement delccountry;	
	
	@FindBy(locator="bookflight.credfname.text")
	private QAFWebElement credfname;
	
	public QAFWebElement getCredfname() {
		return credfname;
	}

	public QAFWebElement getCredmname() {
		return credmname;
	}

	public QAFWebElement getCredlname() {
		return credlname;
	}

	public QAFWebElement getCredcard() {
		return credcard;
	}

	public QAFWebElement getBtotalprice() {
		return btotalprice;
	}

	@FindBy(locator="bookflight.credmname.text")
	private QAFWebElement credmname;
	
	@FindBy(locator="bookflight.credlname.text")
	private QAFWebElement credlname;	
	
	@FindBy(locator="bookflight.credcard.text")
	private QAFWebElement credcard;	
			
	public QAFWebElement getCreditcard() {
		return creditcard;
	}

	public QAFWebElement getBillingaddress() {
		return billingaddress;
	}

	public QAFWebElement getBillingcity() {
		return billingcity;
	}

	public QAFWebElement getBillingstate() {
		return billingstate;
	}

	public QAFWebElement getBillingzip() {
		return billingzip;
	}

	public QAFWebElement getBillingcountry() {
		return billingcountry;
	}

	public QAFWebElement getDeladdress() {
		return deladdress;
	}

	public QAFWebElement getDelcity() {
		return delcity;
	}

	public QAFWebElement getDelstate() {
		return delstate;
	}

	public QAFWebElement getDelzip() {
		return delzip;
	}

	public QAFWebElement getDelccountry() {
		return delccountry;
	}

	public QAFWebElement getReturnbutton() {
		return returnbutton;
	}
	
	@FindBy(locator="selectflight.continue.button")
	private QAFWebElement selectflights;
	
	@FindBy(locator="bookflight.securepurchase.button")
	private QAFWebElement buyFlights;
	
	@FindBy(locator="flightconfirmation.logout.button")
	private QAFWebElement logout;
	
    public QAFWebElement getLogout() {
		return logout;
	}

	public QAFWebElement getBuyFlights() {
		return buyFlights;
	}

	public QAFWebElement getSelectflights() {
		return selectflights;
	}
    
    @FindBy(locator="bookflight.firstname.text")
    private QAFWebElement passfname;
    
    
    @FindBy(locator="bookflight.lastname.text")
    private QAFWebElement passlname;
    
    
	public QAFWebElement getPassfname() {
		return passfname;
	}

	public QAFWebElement getPasslname() {
		return passlname;
	}
	 	
	@FindBy(locator="bookflight.tax.label")
	private QAFWebElement tax;
	
	public QAFWebElement getTax() {
	return tax;
	}
    @FindBy(locator="bookflight.totalprice.label")
    private QAFWebElement btotalprice;
    
	public QAFWebElement getBookTotalprice() {
		return btotalprice;
	}
	
	
	
   
	            
	@FindBy(locator="bookflight.departuredate.label")  
	private QAFWebElement departuredate;   
	                         
	                  
	@FindBy(locator="bookflight.departureclass.label")    
	private QAFWebElement departureclass;      
	               
	@FindBy(locator="bookflight.departurerate.label")   
	private QAFWebElement departurerate;                          


 
	                       
	@FindBy(locator="bookflight.returndate.label")     
	private QAFWebElement retrundate;   
	                       

	                    
	@FindBy(locator="bookflight.returnclass.label")   
	private QAFWebElement returnclass; 
	                         
	@FindBy(locator="bookflight.returnrate.label")                                 
	private QAFWebElement returnrate; 

	public QAFWebElement getDeparturedate() {
		return departuredate;
	}

	public QAFWebElement getDepartureclass() {
		return departureclass;
	}

	public QAFWebElement getDeparturerate() {
		return departurerate;
	}

	public QAFWebElement getRetrundate() {
		return retrundate;
	}

	public QAFWebElement getReturnclass() {
		return returnclass;
	}

	public QAFWebElement getReturnrate() {
		return returnrate;
	}

	@QAFTestStep(description="input different set of data")
	   public void validateinputfields(Map<String,String>m)
	   {
		dateinput(m);
		getDepartbutton().click();
		getReturnbutton().click();
		getSelectflights().click();
		getPassfname().sendKeys(m.get("fname"));
		getPasslname().sendKeys(m.get("lname"));
	   }
	  
	@QAFTestStep(description="entering details for booking page")
	public void bookFlight(Map<String,String>m)
	{
		getPassfname().sendKeys(m.get("Firstname"));
		getPasslname().sendKeys(m.get("Lastname"));
		getCredcard().sendKeys(m.get("creditcardnumber"));
		getCredfname().sendKeys(m.get("credfirstname"));
		getCredmname().sendKeys(m.get("credmidname"));
		getCredlname().sendKeys(m.get("credlastname"));
			
    }
	@FindBy(locator="bookflight.expirymonth.dropdown")
	private QAFWebElement credexpmonth;
	
	
	@FindBy(locator="bookflight.expiryyear.dropdown")
	private QAFWebElement credexpyear;
	
	public QAFWebElement getCredexpmonth() {
		return credexpmonth;
	}

	public QAFWebElement getCredexpyear() {
		return credexpyear;
	}

	public String getTaxtoNext()
	{
		return getTax().getText();
	}
	public String getprice()
	{
		return getBookTotalprice().getText();
	}
	
	@QAFTestStep(description="input different set of data")
	   public void inputcreditCard(Map<String,String>m)
	   {
		dateinput(m);
		getDepartbutton().click();
		getReturnbutton().click();
		getSelectflights().click();
		getCredcard().sendKeys(m.get("creditcardnumber"));
	   }
	
	@QAFTestStep(description="expiry date validation")
	public void expirydatevalidation(Map<String,String>m)
	{
		dateinput(m);
		getDepartbutton().click();
		getReturnbutton().click();
		getSelectflights().click();
		getCredcard().sendKeys(m.get("creditcardnumber"));
		creditexpdate(m.get("value"));
		
	}
	
	public void creditexpdate(String bool){
		
		if(bool.equals("true"))
		{
			 LocalDate l=LocalDate.now();
			 
			 if(l.getMonthValue()<10 && l.getMonthValue()>0)
				{
					String monthvale="0"+l.getMonthValue();
					(new Select(getCredexpmonth())).selectByVisibleText(monthvale);
					
				}
			      else 
			      { 
			    	(new Select(getCredexpmonth())).selectByIndex(l.getMonthValue());
			      }
			 (new Select(getCredexpyear())).selectByIndex(11);
		}
		else if(bool.equals("false"))
		{
			LocalDate past=pastDate(48);
			if(past.getMonthValue()<10 && past.getMonthValue()>0)
				{
					String monthvale="0"+past.getMonthValue();
					(new Select(getCredexpmonth())).selectByVisibleText(monthvale);
					
				}
			      else 
			      { 
			    	 
			    	  (new Select(getCredexpmonth())).selectByIndex(past.getMonthValue());
			      }
			(new Select(getCredexpyear())).selectByIndex(10);
		}
		else if(bool.equals("None"))
		{
			(new Select(getCredexpmonth())).selectByVisibleText("None");
			(new Select(getCredexpyear())).selectByVisibleText("None");
		}
		
	}
	
	public void alertdataverification(Map<String,String>m)
	{
		dateinput(m);
		getDepartbutton().click();
		getReturnbutton().click();
		getSelectflights().click();
		(new Select(getDelccountry())).selectByVisibleText(m.get("delcountry"));
	}
	
	public boolean getAlert() {
		boolean flag=true;
		String expectedmessage = "You have chosen a mailing location outside of the United States and its territories."
				+ " An additional charge of $6.5 will be added as mailing charge.";
		  try {
		        Alert alert = driver.switchTo().alert();
		        String alertText = alert.getText();
		        if(expectedmessage.equals(alertText)) {
		        alert.accept();
		        flag=true;
		        }
		        else 
		        	flag=false;
		    } catch (NoAlertPresentException e) {
		        e.printStackTrace();
		    }
		  return flag;
	}


}
