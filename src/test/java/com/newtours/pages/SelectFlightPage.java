package com.newtours.pages;

import java.util.List;
import java.util.Map;

import org.jboss.netty.util.internal.SystemPropertyUtil;

import com.newtours.components.FlightComponent;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class SelectFlightPage extends FlightFinder {
	
	@FindBy(locator="selectflight.table.row")
	private List<FlightComponent> table;
	
	public List<FlightComponent> getTable() {
	return table;
	}

	@FindBy(locator="selectflight.continue.button")
	private QAFWebElement selectflights;
	
	
    public QAFWebElement getSelectflights() {
		return selectflights;
	}

	public void selectFlight(String title)
    {
		List<FlightComponent>depretcomp=getTable();
		int index;
		for(FlightComponent depret:depretcomp)
		{

			for(QAFWebElement dep:depret.getFlightname())
			{ 
				if(dep.getText().equals(title))
				{
				    index=depret.getFlightname().indexOf(dep);
					depret.getFlight().get(index).click();
				}
			
			}
		}

    }
	public QAFWebElement selectRate(String title)
    {
		List<FlightComponent>depretcomp=getTable();
		QAFWebElement xpath = null;
		for(FlightComponent depret:depretcomp)
		{
			for(QAFWebElement dep:depret.getFlightname())
			{ 
				if(dep.getText().equals(title))
				{
					int index=depret.getFlightname().indexOf(dep);
					xpath=depret.getFlightprice().get(index);
					break;
				}
			}	
			break;
			
		}
		return xpath;
    }
	public String getRate(String text)
	{
		
		StringBuilder s=new StringBuilder();
		for(Character c:text.toCharArray())
		{
		 if(Character.isDigit(c))
			 s.append(c);
        else
        	continue;
		}
		return s.toString();
	}
	public void selectFlight(Map<String,String>m)
	{
		selectFlight(m.get("departure"));		
		selectFlight(m.get("return"));
		//getSelectflights().click();
	}
}


