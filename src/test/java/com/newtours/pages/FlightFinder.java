package com.newtours.pages;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.openqa.selenium.support.ui.Select;

import com.newtours.bean.FlightFinderBean;
import com.newtours.bean.FormRegisterdataBean;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;

public class FlightFinder extends WebDriverBaseTestPage<WebDriverTestPage>{
	@FindBy(locator="login.username.text")
	private QAFWebElement username;
	
	@FindBy(locator="login.password.text")
	private QAFWebElement password;
	
	@FindBy(locator="login.login.btn")
	private QAFWebElement login;
	

	public QAFWebElement getUsername() {
		return username;
	}

	public QAFWebElement getPassword() {
		return password;
	}

	public QAFWebElement getLogin() {
		return login;
	}

	@QAFTestStep(description="Login")
	public void doLogin(String uname,String password)
	{
		getUsername().sendKeys(uname);
		getPassword().sendKeys(password);
		getLogin().click();
	}


	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");
		
	}
	
	@FindBy(locator="flightfinder.triptype.radio")
	private QAFWebElement triptype;
	
	@FindBy(locator="flightfinder.nopassenger.dropdown")
	private QAFWebElement nopassenger;
	
	@FindBy(locator="flightfinder.depfrom.dropdown")
	private QAFWebElement depfrom;
	
	@FindBy(locator="flightfinder.depmonth.dropdown")
	private QAFWebElement depmonth;
	
	@FindBy(locator="flightfinder.depday.dropdown")
	private QAFWebElement depday;
	
	@FindBy(locator="flightfinder.tomonth.dropdown")
	private QAFWebElement tomonth;
	
	@FindBy(locator="flightfinder.today.dropdown")
	private QAFWebElement today;
	
	@FindBy(locator="flightfinder.depto.dropdown")
	private QAFWebElement depto;
	
	@FindBy(locator="flightfinder.serviceclass.radio")
	private QAFWebElement servclass;
	
	@FindBy(locator="flightfinder.airline.dropdown")
	private QAFWebElement airline;
		
	@FindBy(locator="flightfinder.continue.button")
	private QAFWebElement flightcontinue;
	
	public QAFWebElement getTriptype() {
		return triptype;
	}
	public QAFWebElement getNopassenger() {
		return nopassenger;
	}
	public QAFWebElement getDepfrom() {
		return depfrom;
	}
	public QAFWebElement getDepmonth() {
		return depmonth;
	}
	public QAFWebElement getDepday() {
		return depday;
	}
	public QAFWebElement getTomonth() {
		return tomonth;
	}
	public QAFWebElement getToday() {
		return today;
	}
	public QAFWebElement getDepto() {
		return depto;
	}
	public QAFWebElement getServclass() {
		return servclass;
	}
	public QAFWebElement getAirline() {
		return airline;
	}
	public QAFWebElement getFlightcontinue() {
		return flightcontinue;
	}
	 
	public LocalDate futureDate(int nodays)
	 {
	  LocalDate l=LocalDate.now();
      LocalDate mydate=l.plusDays(nodays);
      return mydate;
     }
	public LocalDate pastDate(int nodays)
	 {
	  LocalDate l=LocalDate.now();
     LocalDate mydate=l.minusDays(nodays);
     return mydate;
    }
    public String getMonthName(String s)
    {
    	  String monthname = s.substring(0,1).toUpperCase() + s.substring(1).toLowerCase();
    	   return monthname;
    }
    @QAFTestStep(description="Entering the flight details")
	public void findFlight()
	{
		FlightFinderBean fb=new FlightFinderBean();
		getTriptype().click();
		getServclass().click();
		fb.fillFromConfig("data.flight.flightdetails");
		fb.fillUiElements();
		(new Select(getDepmonth())).selectByVisibleText(getMonthName(futureDate(1).getMonth().toString()));
		(new Select(getDepday())).selectByVisibleText(Integer.toString(futureDate(1).getDayOfMonth()));
		(new Select(getTomonth())).selectByVisibleText(getMonthName(futureDate(6).getMonth().toString()));
		(new Select(getToday())).selectByVisibleText(Integer.toString(futureDate(6).getDayOfMonth()));
}
    
    @QAFTestStep(description="Entering the flight details with same departure and return")
   	public void findFlights()
   	{
   		FlightFinderBean fb=new FlightFinderBean();
   		getTriptype().click();
   		getServclass().click();
   		fb.fillFromConfig("data.flight.flightdetail");
   		fb.fillUiElements();
   		getDepto().sendKeys((new Select(getDepfrom())).getFirstSelectedOption().getText());
   		(new Select(getDepmonth())).selectByVisibleText(getMonthName(futureDate(1).getMonth().toString()));
   		(new Select(getDepday())).selectByVisibleText(Integer.toString(futureDate(1).getDayOfMonth()));
   		(new Select(getTomonth())).selectByVisibleText(getMonthName(futureDate(6).getMonth().toString()));
   		(new Select(getToday())).selectByVisibleText(Integer.toString(futureDate(6).getDayOfMonth()));
   		getDepartureDate();
   		getReturnDate();
   		getFlightcontinue().click();
   }
    
    @QAFTestStep(description="Departure date")
   	public String getDepartureDate()
   	{
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/YYYY"); 
        return formatter.format(futureDate(1)); 
   	}
    @QAFTestStep(description="Return date")
   	public String getReturnDate()
   	{
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/YYYY"); 
        return formatter.format(futureDate(6)); 
   	}
    
    
    @QAFTestStep(description="Validating departure and return dates")
    public void dateinput(Map<String,String> data)
    {
    	//driver.waitForWindowTitle("Find a Flight: Mercury Tours: ", 30);
		getNopassenger().sendKeys(data.get("nopassenger"));
		getDepfrom().sendKeys(data.get("depfrom"));
		getAirline().sendKeys(data.get("airline"));
		getDepto().sendKeys(data.get("depto"));
		getDepmonth().sendKeys(data.get("depmonth"));
		getDepday().sendKeys(data.get("depday"));
		getTomonth().sendKeys(data.get("tomonth"));
		getToday().sendKeys(data.get("today"));
		getFlightcontinue().click();
    }
    

  }
