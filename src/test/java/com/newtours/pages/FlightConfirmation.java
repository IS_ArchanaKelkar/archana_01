package com.newtours.pages;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FlightConfirmation extends FieldValidationBookFlightPage {
	
	@FindBy(locator="flightconfirmation.departing.label")
	private QAFWebElement departing;
	
	@FindBy(locator="flightconfirmation.return.label")
	private QAFWebElement returning;
	
	@FindBy(locator="flightconfirmation.noofpassenger.label")
	private QAFWebElement noofpassengers;
	
	@FindBy(locator="flightconfirmation.billaddress.label")
	private QAFWebElement billingAddress;
	
	@FindBy(locator="flightconfirmation.deliaddress.label")
	private QAFWebElement deladdress;
	
	@FindBy(locator="flightconfirmation.tax.label")
	private QAFWebElement taxinfo;
	
	@FindBy(locator="flightconfirmation.totprice.label")
	private QAFWebElement totprice;

	public QAFWebElement getDeliaddress() {
		return deladdress;
	}

	public QAFWebElement getTaxinfo() {
		return taxinfo;
	}

	public QAFWebElement getTotalprice() {
		return totprice;
	}

	public QAFWebElement getDeparting() {
		return departing;
	}

	public QAFWebElement getReturning() {
		return returning;
	}

	public QAFWebElement getNoofpassengers() {
		return noofpassengers;
	}

	public QAFWebElement getBillingAddress() {
		return billingAddress;
	}
    
}
