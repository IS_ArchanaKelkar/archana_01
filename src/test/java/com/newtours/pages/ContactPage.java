package com.newtours.pages;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class ContactPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator="contact.message.label")
	private QAFWebElement contactmsg;
	
	public QAFWebElement getContactmsg() {
		return contactmsg;
	}


	public QAFWebElement getBacktobutton() {
		return backtobutton;
	}
    
	@FindBy(locator="homepage.contact.btn")
	private QAFWebElement contact;
	
	public QAFWebElement getContact() {
		return contact;
	}

	@FindBy(locator="contact.backto.button")
	private QAFWebElement backtobutton;
	
	@QAFTestStep(description="Back to button verification")
	public void doClick()
	{
	getContact().click();
	String label="This section of our web site is currently under construction. Sorry for any inconvenience.";
	getContactmsg().assertPresent(label);
	getBacktobutton().givenPresent();
	getBacktobutton().click();
	
	}
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
}
