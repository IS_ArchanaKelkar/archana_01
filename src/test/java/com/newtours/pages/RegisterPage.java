package com.newtours.pages;



import java.util.Map;

import com.google.common.base.Verify;
import com.newtours.bean.FormRegisterdataBean;
import com.newtours.bean.RegisterBean;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;


public class RegisterPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator="homepage.register.button")
	private QAFWebElement registerbutton;
	
	@FindBy(locator="register.firstname.text")
	private QAFWebElement firstname;
	
	@FindBy(locator="register.lastname.text")
	private QAFWebElement lastname;
	
	@FindBy(locator="register.phone.text")
	private QAFWebElement phone;
	
	@FindBy(locator="register.email.text")
	private QAFWebElement email;
	
	@FindBy(locator="register.address1.text")
	private QAFWebElement address1;
	
	@FindBy(locator="register.address2.text")
	private QAFWebElement address2;
	
	@FindBy(locator="register.city.text")
	private QAFWebElement city;
	
	@FindBy(locator="register.state.text")
	private QAFWebElement state;
	
	@FindBy(locator="register.postalcode.text")
	private QAFWebElement postalcode;
	
	@FindBy(locator="register.country.text")
	private QAFWebElement country;
	
	public QAFWebElement getCountry() {
		return country;
	}


	@FindBy(locator="register.username.text")
	private QAFWebElement username;
	
	@FindBy(locator="register.password.text")
	private QAFWebElement password;
	
	@FindBy(locator="register.confpassword.text")
	private QAFWebElement confpassword;
	
	public QAFWebElement getConfpassword() {
		return confpassword;
	}


	@FindBy(locator="register.register.button")
	private QAFWebElement register;
	
	
	@FindBy(locator="register.message1.label")
	private QAFWebElement message1;
	
	@FindBy(locator="register.message2.label")
	private QAFWebElement message2;
	
	
	public QAFWebElement getMessage1() {
		return message1;
	}


	public QAFWebElement getMessage2() {
		return message2;
	}


	public QAFWebElement getRegisterbutton() {
		return registerbutton;
	}


	public QAFWebElement getFirstname() {
		return firstname;
	}


	public QAFWebElement getLastname() {
		return lastname;
	}


	public QAFWebElement getPhone() {
		return phone;
	}


	public QAFWebElement getEmail() {
		return email;
	}


	public QAFWebElement getAddress1() {
		return address1;
	}


	public QAFWebElement getAddress2() {
		return address2;
	}


	public QAFWebElement getCity() {
		return city;
	}


	public QAFWebElement getState() {
		return state;
	}


	public QAFWebElement getPostalcode() {
		return postalcode;
	}


	public QAFWebElement getUsername() {
		return username;
	}


	public QAFWebElement getPassword() {
		return password;
	}


	public QAFWebElement getRegister() {
		return register;
	}
	
	public void registerUser()
	{
		getRegisterbutton().click();
		FormRegisterdataBean rb=new FormRegisterdataBean();
		rb.fillFromConfig("data.register.user");
		rb.fillUiElements();
		getRegister().click();	
	}
	
	public void registerInvalidUser(Map<String,String>data)
    {
		getRegisterbutton().click();
		getFirstname().sendKeys(data.get("Firstname"));
		getLastname().sendKeys(data.get("Lastname"));
		getPhone().sendKeys(data.get("Phone"));
		getEmail().sendKeys(data.get("Email"));
		getAddress1().sendKeys(data.get("Address1"));
		getAddress2().sendKeys(data.get("Address2"));
		getCity().sendKeys(data.get("City"));
		getState().sendKeys(data.get("State"));
		getPostalcode().sendKeys(data.get("Postalcode"));
		getCountry().sendKeys(data.get("Country"));
		getUsername().sendKeys(data.get("Username"));
		getPassword().sendKeys(data.get("Password"));
		getConfpassword().sendKeys(data.get("ConfirmPassword"));
		getRegister().click();
    }
			
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");
		
	}

}
