package com.newtours.pages;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class SignOnPage extends WebDriverBaseTestPage<WebDriverTestPage> {
   
	@FindBy(locator="signon.username.text")
	private QAFWebElement username;
	
	@FindBy(locator="signon.password.text")
	private QAFWebElement password;
	
	@FindBy(locator="signon.login.btn")
	private QAFWebElement login;
	
	@FindBy(locator="homepage.signon.btn")
	private QAFWebElement signon;

	public QAFWebElement getUsername() {
		return username;
	}

	public QAFWebElement getPassword() {
		return password;
	}

	public QAFWebElement getLogin() {
		return login;
	}
	
	public QAFWebElement getSignon() {
		return signon;
	}
    
	@QAFTestStep(description="Login")
	public void doLogin(String uname,String pwd)
	{
		getSignon().click();
		getUsername().sendKeys(uname);
		getPassword().sendKeys(pwd);
		getLogin().click();
	}
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
}
