package com.newtours.pages;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class LoginPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator="login.username.text")
	private QAFWebElement username;
	
	@FindBy(locator="login.password.text")
	private QAFWebElement password;
	
	@FindBy(locator="login.login.btn")
	private QAFWebElement login;
	

	public QAFWebElement getUsername() {
		return username;
	}

	public QAFWebElement getPassword() {
		return password;
	}

	public QAFWebElement getLogin() {
		return login;
	}

	@QAFTestStep(description="Login")
	public void doLogin(String uname,String pwd)
	{
		getUsername().sendKeys(uname);
		getPassword().sendKeys(pwd);
		getLogin().click();
		//Reporter.log("success");
	}


	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");
		
	}

}
